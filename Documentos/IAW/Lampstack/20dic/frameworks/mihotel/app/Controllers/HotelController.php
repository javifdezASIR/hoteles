<?php

namespace App\Controllers;
use App\Models\HotelModel;


class HotelController extends BaseController
{
    public function listarHotel()
    {
        $data['title'] = 'Listado de Hoteles';
        $hotelModel = new HotelModel();
        $data['hoteles'] = $hotelModel->findAll();
        /*
        echo "<pre>";
            print_r ($hotelModel->findAll());
        echo "</pre>";
        */
        return view('hoteles/listadoHoteles', $data);
    }
    
    public function insertarHotel(){
        $data['title'] = 'Crea un Nuevo Hotel';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { //la primera vez
           return view('hoteles/creaHotel', $data); 
        } else {
            $hotelNuevo = $this->request->getPost();
            
            $hotelModel = new HotelModel();
            if ($hotelModel->insert($hotelNuevo) === false){
               $data['errores'] = $hotelModel->errors(); 
               return view('hoteles/creaHotel', $data);  
            }
            
        }
        
        return redirect()->to('listado/hoteles');
    }
    
    
    public function borrarHotel($id){
        $hotelModel = new HotelModel();
        $data['hotel'] = $hotelModel->where('id', $id)->delete();
        $data['title'] = 'Borar Hotel';
        return redirect()->back();
    }
    
    
    public function editaHotel($id){
        helper('form');
        $data['title'] = 'Editar Hotel';
        $hotelModel = new HotelModel();
        $data['hotel'] = $hotelModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            return view('hoteles/editaHotel',$data);
        } else {
            $hotel = $this->request->getPost();
            unset($hotel['botoncito']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
            //$hotelModel->setValidationRule('NIA', 'required|numeric|min_length[8]');
            if ($hotelModel->update($id, $hotel) === false){
                //hay un error al actualizar
                $data['errores'] = $hotelModel->errors();
                return view('hoteles/editaHotel',$data);
            }
        }        
        return redirect()->to('listado/hoteles');
    }


    
    
    
    
}
