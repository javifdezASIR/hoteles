<?php

namespace App\Models;
use CodeIgniter\Model;

class HotelModel extends Model{ 
    protected $table = 'hoteles';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['id', 'nombre', 'localidad', 'direccion', 'cp', 'email', 'descripcion'];
    
    protected $validationRules = [
        'nombre'    => 'required',
        'localidad' => 'required',
        'direccion' => 'required',
        'cp'        => 'required|numeric|max_length[7]',
        'email'     => 'required|valid_email',
        'descripcion' => 'required|max_length[10000]',
        
    ];
    
    
     
}
