<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
/*************************************************/
/*GRUPOS                                         */
/*************************************************/
$routes->get('/listado/grupos', 'MostrarController::grupos'); // MOSTRAR GRUPOS
$routes->get('/listado/grupoAlumnos/(:alphanum)', 'MostrarController::grupoAlumno/$1'); // MOSTRAR LOS ALUMNOS DE X GRUPO

$routes->get('/borrar/grupo/(:alphanum)', 'BorrarController::grupo/$1'); // BORRAR GRUPO

$routes->get('/insertar/grupo', 'InsertarController::formGrupos'); // INSERTAR GRUPOS
$routes->post('/insertar/creaGrupo', 'InsertarController::insertaGrupo'); // INSERTAR GRUPOS

$routes->get('editar/grupo/(:num)', 'EditarController::grupos/$1'); // EDITAR GRUPO
$routes->post('editar/editogrupo/(:num)', 'EditarController::editoGrupo/$1'); // EDITAR GRUPO

/*************************************************/
/*ALUMNOS                                        */
/*************************************************/
$routes->get('/borrar/alumno/(:alphanum)', 'BorrarController::alumno/$1'); // BORRAR ALUMNO

$routes->get('/insertar/alumno', 'InsertarController::formAlumnos'); // INSERTAR ALUMNO
$routes->post('/insertar/creaAlumno', 'InsertarController::insertaAlumno'); // INSERTAR ALUMNO

$routes->get('editar/alumnos/(:num)', 'EditarController::alumnos/$1'); // EDITAR ALUMNO
$routes->post('editar/editoAlumno/(:num)', 'EditarController::editoAlumno/$1'); // EDITAR ALUMNO

/*************************************************/
/*HOTELES                                        */
/*************************************************/
$routes->get('/listado/hoteles', 'HotelController::listarHotel'); // MOSTRAR HOTELES

$routes->get('/insertar/hotel', 'HotelController::insertarHotel'); // INSERTAR HOTELES
$routes->post('/insertar/hotel', 'HotelController::insertarHotel'); // INSERTAR HOTELES

$routes->get('/borrar/hotel/(:num)', 'HotelController::borrarHotel/$1'); // BORRAR HOTELES

$routes->get('/hotel/editaHotel/(:num)', 'HotelController::editaHotel/$1'); // EDITAR HOTELES
$routes->post('/hotel/editaHotel/(:num)', 'HotelController::editaHotel/$1'); // EDITAR HOTELES


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
