
<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>


    <?= form_open('/hotel/editaHotel/' . $hotel->id) ?>
    <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('nombre', $hotel->nombre, ['class' => 'form_control col-9', 'id' => 'nombre']) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('Descripción:', 'descripcion', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('descripcion', $hotel->descripcion, ['class' => 'form_control col-9', 'id' => 'descripcion', "required"=>"required"]) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('Localidad:', 'localidad', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('localidad', $hotel->localidad, ['class' => 'form_control col-9', 'id' => 'localidad', "required"=>"required"]) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('Direccón:', 'direccion', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('direccion', $hotel->direccion, ['class' => 'form_control col-9', 'id' => 'direccion', "required"=>"required"]) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('CP:', 'cp', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('cp', $hotel->cp, ['class' => 'form_control col-9', 'id' => 'cp', 'type' =>'number', 'maxlength' =>'7', "required"=>"required"]) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('Email:', 'email', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('email', $hotel->email, ['class' => 'form_control col-9', 'id' => 'email', 'type' =>'email', "required"=>"required"]) ?>
        </div>
    </div>



    <div class="form-group row">
        <div class="col-sm-10">
            <?= form_submit('botoncito', 'Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= form_close() ?>
<?= $this->endSection() ?>
