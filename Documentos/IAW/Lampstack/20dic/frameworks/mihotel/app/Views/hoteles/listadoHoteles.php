<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<a class="btn btn-primary mb-3" href="<?=site_url('insertar/hotel/')?>">Insertar</a> <br>

    <table class="table table-striped" id="myTable">
        <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Localidad
                </th>
                <th>
                    Dirección
                </th>
                <th>
                    CP
                </th>
                <th>
                    Email
                </th>
                <th>
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hoteles as $hotel): ?>
                <tr>
                    <td>
                        <?= $hotel->nombre ?>
                    </td>
                    <td>
                        <?= $hotel->localidad ?>
                    </td>
                    <td>
                        <?= $hotel->direccion ?>
                    </td>
                    <td>
                        <?= $hotel->cp ?>
                    </td>
                    <td>
                        <?= $hotel->email ?>
                    </td>
                    
                    <td class="text-right">
                        <a href="<?=site_url('hotel/editaHotel/'.$hotel->id)?>">
                            <span class="bi bi-pencil-square" title="Editar el hotel"></span>
                        </a>
                        <a href="<?=site_url('borrar/hotel/'.$hotel->id)?>" onclick="ventanita()">
                            <span class="bi bi-eraser-fill" title="Eliminar el hotel"></span>
                        </a>                         
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<script>
    function ventanita() {  
        alert("Este grupo se eliminará");  
    }  
</script>


<?= $this->endSection() ?>


