<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>


    <form action="<?=site_url('insertar/hotel/')?>" method="post">
        <div class="form-group">
            <?= form_label('Nombre:', 'nombre', ['class'=>'col-2'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre', "required"=>"required"]) ?>
        </div>
        <div class="form-group">
            <?= form_label('Descripcion:', 'descripcion', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('descripcion',set_value('descripcion',''),['class'=>'form_control col-9', 'id'=>'descripcion', "required"=>"required"]) ?>
        </div>
        <div class="form-group">
            <?= form_label('Localidad:', 'localidad', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('localidad',set_value('localidad',''),['class'=>'form_control col-9', 'id'=>'localidad', "required"=>"required"]) ?>
        </div>
        <div class="form-group">
            <?= form_label('Dirección:', 'direccion', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('direccion',set_value('direccion',''),['class'=>'form_control col-9', 'id'=>'direccion', "required"=>"required"]) ?>
        </div>
        <div class="form-group">
            <?= form_label('CP:', 'cp', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('cp',set_value('cp',''),['class'=>'form_control col-9', 'id'=>'cp', "required"=>"required", 'type' =>'number', 'maxlength' =>'7']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Email:', 'email', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('email',set_value('email','tocorreo@dominio.com'),['class'=>'form_control col-9', 'id'=>'email', "required"=>"required", 'type' =>'email']) ?>
        </div>

        <input class="btn btn-primary" type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>
