<?php


?>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('createAlumno')?>" method="post">
            <label for="id" class="text-success">Id:</label>
            <input type="number" name="id" value="id" id="id" />
            <br>
            <label for="nif" class="text-success">nif:</label>
            <input type="text" name="nif" value="nif" id="nif" />
            <br>
            <label for="nombre" class="text-success">Nombre:</label>
            <input type="text" name="nombre" value="Nombre" id="nombre" />
            <br>
            <label for="apellido1" class="text-success">Apellido 1:</label>
            <input type="text" name="apellido1" value="Primer Apellido" id="apellido1" />
            <br>
            <label for="apellido2" class="text-success">Apellido 2:</label>
            <input type="text" name="apellido2" value="Segundo Apellido" id="apellido2" />
            <br>
            <label for="tipotasa" class="text-success">tipotasa:</label>
            <input type="date" name="tipotasa" value="tipotasa" id="tipotasa" />
            <br>
            <label for="ciclo" class="text-success">ciclo:</label>
            <input type="text" name="ciclo" value="ciclo" id="ciclo" />
            <br>
            <label for="email" class="text-success">Email</label>
            <input type="email" name="email" value="Segundo Apellido" id="email" />
            <br>
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>
    
