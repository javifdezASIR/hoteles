<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <!--<!-- EL CSS QUE HE PUESTO, JAVIER FERNANDEZ -->
        <style>
            .fcc-btn {
                background-color: #B5B2B2;
                color: white;
                padding: 15px 25px;
                text-decoration: none;
            }

            .fcc-btn:hover {
                background-color: #000000;
            }
            
            a:hover {
                color:white;
            }
        </style>
        
        
        <title><?=$title?></title>
    </head>
    <body>
         <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('')?>" method="post">
        <table class="table table-striped">
            <?php foreach ($resultado as $pau): ?>
                <tr>
                    <td>
                        <?= $pau->id ?>
                    </td>

                    <td>
                        <?= $pau->nif ?>
                    </td>
                    
                    <td>
                        <?= $pau->apellido1 ?>
                    </td>
                    
                    <td>
                        <?= $pau->apellido2 ?>
                    </td>
                    
                    <td>
                        <?= $pau->nombre ?>
                    </td>
                    
                    <td>
                        <?= $pau->email ?>
                    </td>
                    
                    <td>
                        <?= $pau->ciclo ?>
                    </td>
                    
                    <td>
                        <?= $pau->tipo_tasa ?>
                    </td>
                    
                    <td>   
                        <a class="fcc-btn" href="http://localhost:8080/8nov/index.php/borrar/nif/<?= $pau->nif ?>">¿Desea borrar?</a>
                    </td>
                    

                    
                </tr>

            <?php endforeach; ?>
        </table>
        </form>
    </body>
</html>

