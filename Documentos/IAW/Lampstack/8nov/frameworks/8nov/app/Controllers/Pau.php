<?php

namespace App\Controllers;
use App\Models\PauModel;


class Pau extends BaseController
{
    public function muestraPau()
    {
        $data['title'] = 'Listado de la Tabla PAU';
        $pauModel = new \App\Models\PauModel();
        //$data['resultado'] = $pauModel->findAll();
        
        $data ['resultado' ] = $pauModel
	->select ('p.id, p.nif, p.nombre, p.apellido1, p.apellido2, p.email, c.nombre as ciclo, p.tipo_tasa')
	->from ('pau as p')
	->join('ciclos as c','c.id = p.ciclo','left') 
        ->distinct('p.id')
	->findAll();
        
        return view('pau/lista',$data);
        
        $pauModel->delete(['p.nif' => $pau]);

    }
    

}