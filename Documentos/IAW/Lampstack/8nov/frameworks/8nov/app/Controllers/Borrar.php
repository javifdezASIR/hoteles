<?php

namespace App\Controllers;
use App\Models\PauModel;


class Borrar extends BaseController
{
    public function borrarPau($pau)
    {
        $data['title'] = '¿Estás seguro que quieres borrar?';
        $pauModel = new \App\Models\PauModel();
        
        //$data ['resultado' ] = $pauModel
	//->select ('p.id, p.nif, p.nombre, p.apellido1, p.apellido2, p.email, p.ciclo as ciclo, p.tipo_tasa')
	//->from ('pau as p')
	//->join('ciclos as c','c.id = p.ciclo','left') 
        //->where (['p.nif'=>$pau])
        //->distinct('p.id')
	//->findAll();
        
        $pauModel->delete(['p.nif' => $pau]);
        

                
        return view('borrar/lista',$data);
    }
}


