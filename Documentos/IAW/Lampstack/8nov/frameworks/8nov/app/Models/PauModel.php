<?php

namespace App\Models;
use CodeIgniter\Model;

class PauModel extends Model {
    
    protected $table = 'pau';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    
    protected $allowedFields = [
      'id',
      'nif',
      'nombre',
        'apellido1',
              'apellido2',
              'email',
                      'cilo',
                              'tipo_tasa',
                      'delete_at'

    ];

    
}